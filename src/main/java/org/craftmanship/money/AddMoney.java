package org.craftmanship.money;

import static org.craftmanship.money.BigDecimalMoney.of;

public class AddMoney {

    public static void main(String...args){
        Money op1 = of(args[0]);
        Money op2 = of(args[1]);

        Money result = op1.add(op2);
        System.out.println(result);
    }
}
